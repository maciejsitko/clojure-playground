(ns default.core)
(def filename "suspects.csv")

(def vamp-keys [:name :glitter-index])


(defn str->int
  [str]
  (Integer. str))

(def conversions {:name identity
                  :glitter-index str->int})

(defn convert
  [vamp-key value]
  ((get conversions vamp-key) value))

(defn parse
  [string]
  (map (fn [line] (clojure.string/split line #","))
        (clojure.string/split string #"\n")))

(defn mapify
  [rows]
  (map (fn [unmapped-row]
         (reduce (fn [row-map [key val]]
                   (assoc row-map key (convert key val)))
                 {}
                 (map vector vamp-keys unmapped-row))
         ) rows))

(defn glitter-filter
  [lower records]
  ( into [] (map :name (filter (fn [rec] (>= (:glitter-index rec) lower )) records))))

(def suspects (mapify (parse (slurp filename))))



(defn append
  [result item]
  (if (validate item)
  (into result ( vector(:name item)))
    nil))

(def validators [:name
                 :glitter-index])

(defn validate [hashmap]
  (let [[name glitter-filter] validators
        hashmap hashmap]
  (and (contains? hashmap name ) (contains? hashmap glitter-filter) true)))

(def six-glitter-level (glitter-filter 1 suspects))


(defn join-to
  [hashmap]
  (let [{name :name glitter-filter :glitter-index} hashmap]
  (str name "," glitter-filter "\n")))

(defn suspects->csv
  [csvmap]
  (clojure.string/join "" (map join-to csvmap)))


(append six-glitter-level {:glitter-index 5 :name "Barnett"})

(def converted-back (suspects->csv suspects))
(spit "suspects.csv" converted-back)


