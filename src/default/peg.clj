(ns peg.core
  (require [clojure.set :as set])
  (:gen-class))


(declare successful-move prompt-move game-over query-rows)

(def tri (tri*))

(defn tri*
  ([] (tri* 0 1))
  ([sum n]
    (let [new-res (+ sum n)]
      (cons new-res (lazy-seq(tri* new-res (inc n)))))))

(take 5 (tri*))

(defn triangular?
  [n]
  (= n (last (take-while (fn [compare] (>= n compare)) tri))))

(triangular? 6)

(defn tri-row
  [n]
  (last (take n tri)))

(tri-row 1)

(defn tri-row-num
  [pos]
  (inc (count (take-while (fn [row-num] (> pos row-num)) tri))))

(defn connect
  [board max-pos pos neighbor destination]
  (if(<= destination max-pos)
    (reduce (fn [new-board [p1 p2]]
              (assoc-in new-board [p1 :connections p2] neighbor))
            board
            [[pos destination] [destination pos]])
    board))

(connect {} 15 1 2 4)

(assoc-in {} [:1 :2 :3 :4] 5)

(defn add*
  ([] (add* 0 1))
  ([n] (add* 0 n))
  ([acc n]
  (let [sum (+ acc n)]
  (cons sum (lazy-seq (add* sum n))))))

(take 10 (add*))

(defn connect-right
  [board max-pos pos]
  (let [neighbor (inc pos)
        destination (inc neighbor)]
    (if-not(or (triangular? neighbor) (triangular? pos))
      (connect board max-pos pos neighbor destination)
      board)))

(defn connect-down-left
  [board max-pos pos]
  (let [row (tri-row-num pos)
          neighbor (+ row pos)
          destination (+ 1 row neighbor)]
    (connect board max-pos pos neighbor destination)))

(defn connect-down-right
  [board max-pos pos]
  (let [row (tri-row-num pos)
          neighbor (+ 1 row pos)
          destination (+ 2 row neighbor)]
    (connect board max-pos pos neighbor destination)))

