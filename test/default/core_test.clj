(ns default.core-test
  (:require [clojure.test :refer :all]
            [default.core :refer :all]))

(deftest a-test
  (testing "FIXME, I fail."
    (is (= 0 1))))

(defn loop-test []
  (loop [iteration 24 acc 0]
    (println (str "Iteration " iteration))
    (if (= iteration 0)
      acc
      (recur (dec iteration) (+ acc iteration) ))))

(loop-test)

(defn factorial [num]
  (if (= num 0)
    1
    (* num (factorial (- num 1)))))

(defn factorial-loop [num]
  (loop [n num p 1]
    (if (= n 0)
     p
    (recur (dec n) (* p n) ))))

(factorial 8)
(factorial-loop 8)


(def fibonacci
     (lazy-cat [0 1] (map + (rest fibonacci) fibonacci)))


(take 20 fibonacci)

(defn lazy-cat-test [i]
  (lazy-cat [i] (repeat i i) (lazy-cat-test (inc i))))


(take 20 (lazy-cat-test 1))


;; _________________
;;
;; CLJ4B&T Exercises:
;; _________________


;; Introductory Tasks

(str 1)
(vector 1 2 3)
(list 1 2 3)
(hash-map :1 2 :3 4)
(hash-set 1 2 3 4)

(set [1 2 3])
;; creates hash-set from vector


(defn dec-maker [n1]
  (fn [n2] (- n1 n2)) )

(def dec5 (dec-maker 5))

(dec5 4)

;; ------------------

;; 0. Generalised Symmetrizer:

(defn replacer [part n]
 (let [iterate n
         parts (repeat n part)
       ]
  (map-indexed (fn [i p]
         {:part (str (+ i 1) "-"(:part p))  :size (:size p)})
       parts)))


(defn generalised-symmetrizer [body-parts n]
  (reduce (fn [new-parts part]
          (into new-parts (set (replacer part n))))
          [] body-parts))

(generalised-symmetrizer [{:part "foot" :size 5} {:part "head" :size 10}] 5)

;; 1. Recursive Radial Symmetrizer:

(defn radial-replacer [part]
  (cond (re-find #"^first-" (:part part)) {:part (clojure.string/replace (:part part) #"^first-" "fifth-") :size (:size part) }
        (re-find #"^second-" (:part part)) {:part (clojure.string/replace (:part part) #"^second-" "sixth-") :size (:size part) }
        (re-find #"^third-" (:part part)) {:part (clojure.string/replace (:part part) #"^third-" "seventh-") :size (:size part) }
        (re-find #"^fourth-" (:part part)) {:part (clojure.string/replace (:part part) #"^fourth-" "eighth-") :size (:size part) }
        (re-find #"^fifth-" (:part part)) {:part (clojure.string/replace (:part part) #"^fifth-" "tenth-") :size (:size part) }
   ))

(defn radial-match [part]
  (set [part (radial-replacer part)]))


(defn symmetrize-radial-parts [body-parts]
  (loop [radial-parts body-parts
        new-parts []]
    (if (empty? radial-parts)
       new-parts
    (let [[part & remaining] radial-parts]
      (recur remaining (into new-parts (radial-match part)))))))

(symmetrize-radial-parts [{:part "second-eye" :size 5}
                          {:part "first-leg" :size 10}])

;; 2.Reduce Radial Symmetrizer:


(defn symmetrize-radial-parts-2 [body-parts]
  (reduce (fn [new-parts part]
            (into new-parts (radial-match part)))
            [] body-parts  ))

(symmetrize-radial-parts-2 [{:part "second-eye" :size 5}
                          {:part "first-leg" :size 10}])

;; ---------------------------

(defn even-nums
  ([] (even-nums 0))
  ([n]  (cons n (lazy-seq (even-nums (+ n 2))))))

(take 5 (even-nums))
(take 3 [ 1 2 3 4])
